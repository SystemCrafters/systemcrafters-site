#+title: 5 Reasons to Learn Scheme in 2024
#+video: 3eXK9YZ0NjU

* What is Scheme?

Scheme is a minimal yet incredibly powerful member of the Lisp family of languages.

Because it's a Lisp, it shares the same parenthesis-oriented syntax as other Lisp languages, but it's quite unique compared to Common Lisp or Clojure for example.

In this video, I'm going to give you 5 reasons why I think it's worth your time to learn Scheme this year.

If after the video you're interested to learn it, I'm running a 4 week live instructional course called "Hands-On Guile Scheme for Beginners" starting in February.  You can find a link with more details in the description and show notes below:

https://systemcrafters.net/courses/hands-on-guile-scheme-beginners/

I'll also be making more advanced courses this year about Guile Scheme and other topics, so if you want to be notified about those you can sign up for my newsletter, the link can also be found in the description:

https://systemcrafters.net/newsletter/

Now let's talk about Scheme!

* 1. You will deepen your programming knowledge

Scheme is a great language both for beginners and for intermediate to advanced programmers.

This is because its core language design provides a small set of features that work together to help you build well-crafted, functional programs.

For beginners there's less to learn at first, and the focus on the fundamentals of computation and data structures helps to build a solid foundation for writing good code.

For advanced programmers, there are a few powerful features of the language that will really expand your mind and give you the tools to write code that just isn't possible in other languages.  More on that in a bit.

** Functional Programming

Scheme is centered around functional programming, where the function is the core unit of abstraction.  It's a great first language to learn functional programming because it isn't as strict about everything being "pure functional" with immutable data structures like in other languages.

Where other languages like Java or C++ use classes as the central aspect of your code, Scheme programs use functions that encapsulate their own information and pass them to other functions to compose more sophisticated behavior.

Scheme also promotes recursion as a key aspect for algorithm design.  It's easy to create recursive blocks of code anywhere, without defining a function, to produce elegant algorithms for any kind of iteration even if the data source doesn't have a predetermined length.

* 2. Incredibly powerful features

While keeping a pretty minimal core language, Scheme actually provides a few powerful features that aren't available in many other languages:

** Efficient Recursion

I mentioned before that Scheme often uses recursion to implement algorithms.  In some languages, recursion can be troublesome because when a function calls itself many times, it may exceed the number of function calls that the "call stack" can handle, leading to errors.

Scheme implementations provide a feature called "tail call optimization" which can help you avoid this problem by enabling recursive algorithms to seem more like plain loops to the compiler.

When you become comfortable writing Scheme code you will find many ways to use this feature, even outside of recursive loops.

** "Hygenic" Macros

One of the first things you will hear about Lisp languages is the ability to write macros, or basically functions that can produce new code when they are called.

Lisps that provide a macro capability usually expose it as a simple code templating facility using a feature called "backquoting".  This is just syntactic sugar for creating lists and the compiler can take that list and turn it into executable code at compile time.

The trouble with this approach is that the macro author has to be much more careful not to make a mistake when writing a macro because it's very easy to introduce bugs that can affect program behavior in surprising ways.

Scheme has a different macro design which enables you to give more information to the compiler about how the macro will be used, making it possible for the compiler to manage the environment of the generated code more effectively.

This means that not only will you have less bugs due to your macros, you will usually also get better error messages when you write code using that macro.

** Continuations

First-class continuations are an advanced "control flow" feature of Scheme and one of the most unique things about the language.  "First-class" in this case means that the language provides this as a tool for you to use, not purely as a compiler implementation detail.

I mentioned the idea of a "call stack" before.  Typically when a program starts, the code calls a series of functions which creates a "stack" of calls which eventually returns back to the original location where the program started.

In Scheme programs, it is possible to store the entire call stack and replace it with another one while the program is running.  This stored call stack is called a "continuation" and it is represented as a function that restores the call stack and resumes execution of the code there.

With continuations, it is possible to implement many advanced language features:

- Try/catch style exceptions
- Early returns in recursive search algorithms
- Cooperative multitasking and coroutines
- Go-style channels
- Actor model

Your brain might hurt while learning to use continuations, but that's good!

* 3. Wide variety of Scheme implementations

Scheme is not a single language implementation, it is a language specification with many implementations.

The core language is described by the R*RS series of specifications, each a more refined version of the last.  The most recent is R7RS-small which was finalized in 2013.  This document is well worth reading if you are interested in the language:

https://small.r7rs.org/attachment/r7rs.pdf

There are Scheme implementations for many use cases, all with their own slight differences:

- [[https://www.gnu.org/software/guile/][Guile]]: Used for program extensibility and application development in the GNU ecosystem
- [[https://racket-lang.org/][Racket]]: More than a Scheme; a rich language-building toolkit
- [[https://cisco.github.io/ChezScheme/][Chez]]: Industrial-grade Scheme with a cutting edge implementation
- [[http://gambitscheme.org/][Gambit]]: A Scheme-to-C compiler making it possible to write Scheme code to be deployed almost anywhere
- [[https://scheme.fail/][Loko]]: Write Scheme that compiles to "bare metal"
- [[http://synthcode.com/scheme/chibi][Chibi]]: Embeds directly into C applications to provide Scheme-based scripting

...and [[https://get.scheme.org/][many more]]!

For the purpose of this channel, we focus mainly on *Guile Scheme* which I find to be the most practical and versatile for personal projects.

* 4. You can write any kind of application with it

Scheme is not just an "academic language"!

Just like many other popular languages, there are a wealth of Scheme libraries that make it possible to write pretty much any kind of application you want:

- Terminal apps
- UI apps with GTK, QT, and more
- Web servers and clients
- Games
- Mobile apps ([[http://www.lambdanative.org/][LambdaNative]])

You can even interface directly with low-level code using implementations like Guile Scheme which provide a runtime foreign-function interface (the ability to call C libraries directly).

With the development of [[https://spritely.institute/news/guile-hoot-v010-released.html][Guile Hoot]] by the [[https://spritely.institute][Spritely Institute]], you can now compile Scheme applications to WebAssembly to run in stable versions of Chrome and Firefox!  Hoot places Scheme at the forefront of current WebAssembly compilers with its support for host-managed GC and tail call optimization, so it's worth checking out.

* 5. You will get the full benefit of GNU Guix

Learning Scheme will make it possible for you to master GNU Guix because you will be able to dive into the code and understand how everything works.

Basic Scheme knowledge will certainly make it easier to write system configurations and basic package definitions, but you'll benefit from more experience working with the language to go further and write service definitions and other customizations.  GNU Guix is a great practical way to use Scheme!

If you haven't heard of GNU Guix before, I've made a number of videos on this channel about it including one called "5 Reasons to Try Guix in 2022" (still relevant in 2024 :)

https://www.youtube.com/watch?v=7ZdMRLPZhFs

* Let me know your thoughts!

Now that I've given you my 5 reasons why you should learn Scheme this year, I want to hear from you!

Let me know in the comments below:

- Have you tried Scheme before?  How did it go?
- If you haven't tried Scheme yet, why not?

Don't forget about the Hands-On Guile Scheme Beginners course that I'll be running in February, the link is in the show notes and the description below.

https://systemcrafters.net/courses/hands-on-guile-scheme-beginners/

And for general learning resources about Scheme, check out the official Scheme community site:

https://www.scheme.org/
