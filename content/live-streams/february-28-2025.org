#+title: Crafting a CI Server with Laminar and Guix
#+subtitle: System Crafters Live - February 21, 2025
#+date: [2025-02-21 Fri]
#+video: BUAfiUi4LUw

* News

- Emacs 30.1 has been released!

- Until March 1, I'm giving a $50 discount on my "Hands-On Guile Scheme for Beginners" course!  If you want to learn how to write Scheme like I do in these streams, click the following link to learn more, the discount is automatically applied!

  https://systemcrafters.net/courses/hands-on-guile-scheme-beginners/

* Crafing a CI Server

- Single server for multiple sites
- This server (using Laminar) will also host CI to run builds to auto-deploy on new changes
- Webhooks from Codeberg to the CI server should trigger Laminar jobs for the associate repo
- Set up Laminar with Guile for the job scripts

Check out the configuration we worked on here:

https://codeberg.org/SystemCrafters/server-configurations/src/branch/master/systemcrafters/systems/main-server.scm
