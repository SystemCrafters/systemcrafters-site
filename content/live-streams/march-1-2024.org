#+title: Crafting a Minimal Writing Environment with Emacs
#+subtitle: System Crafters Live - March 1, 2024
#+date: [2024-03-01 Fri]
#+video: yY-qyCYc5GU

* News

- Registration is open for the next "Hands-On Guile Scheme for Beginners" iteration!

  There are a number of improvements to the course this time, so check out the site even if you've looked at it before:

  https://systemcrafters.net/courses/hands-on-guile-scheme-beginners/

  If you register early before *6 AM UTC, Monday, March 4th*, you'll receive an automatic 15% off of the registration price!

- The Spritely Institute is hiring a Scheme hacker!

  This is an exciting opportunity to hack on cutting-edge Scheme software: integrating Guile Hoot and Spritely Goblins!

  https://spritely.institute/jobs/Contract-WASM-integration.html

- Check out this week's SC Newsletter:

  https://systemcrafters.net/newsletter/sc-news-007

  Sign up here: https://systemcrafters.net/newsletter/

* Let's build an Emacs for writing!

We'll use the following packages (mostly by Prot):

- Fontaine: https://protesilaos.com/emacs/fontaine
- Spacious Padding: https://protesilaos.com/emacs/spacious-padding
- Logos: https://protesilaos.com/emacs/logos
- TMR: https://protesilaos.com/emacs/tmr
- wc-mode: https://github.com/bnbeckwith/wc-mode

And maybe others that we find!

Other packages you should try:

- https://github.com/amperser/proselint
- https://github.com/rnkn/olivetti
- https://github.com/alphapapa/hammy.el
- https://github.com/jojojames/dired-sidebar

** The Final Configuration

#+begin_src emacs-lisp

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(setq custom-file (locate-user-emacs-file "custom.el"))
(load custom-file)

;; Get rid of some visual noise
(tool-bar-mode 0)

;; Set up a nice color theme
(use-package ef-themes
  :ensure t
  :config
  (load-theme 'ef-duo-dark t))

(use-package spacious-padding
  :ensure t
  :custom
  (spacious-padding-subtle-mode-line t)
  (spacious-padding-widths
      '( :internal-border-width 30
         :header-line-width 4
         :mode-line-width 10
         :tab-width 4
         :right-divider-width 30
         :scroll-bar-width 8
         :fringe-width 8))
  :config
  (spacious-padding-mode 1))

(use-package fontaine
  :ensure t
  :custom
  (fontaine-presets
   '((regular
      :default-family "JetBrains Mono"
      :default-weight normal
      :default-height 150
      :fixed-pitch-family "JetBrains Mono"
      :fixed-pitch-weight nil ; falls back to :default-weight
      :fixed-pitch-height 1.0
      :variable-pitch-family "Iosevka Aile"
      :variable-pitch-weight normal
      :variable-pitch-height 1.2
      :line-spacing 1)
     (large
      :inherit regular
      :default-height 175
      :variable-pitch-height 1.3)))
  :config
  (add-hook 'enable-theme-functions #'fontaine-apply-current-preset)
  (fontaine-set-preset 'regular))

(use-package org
  :ensure nil)

(use-package variable-pitch-mode
  :ensure nil
  :hook
  (org-mode . variable-pitch-mode))

(use-package visual-line-mode
  :ensure nil
  :hook
  (org-mode . visual-line-mode))

(use-package visual-fill-column
  :ensure t
  :hook (org-mode . visual-fill-column-mode)
  :custom
  (visual-fill-column-center-text t)
  (visual-fill-column-width 110))

(use-package logos
  :ensure t
  :custom
  (logos-outlines-are-pages t)
  (logos-hide-mode-line t))

(defun my/dired-jump-sidebar ()
  (interactive)
  (display-buffer-in-side-window
   (dired-noselect default-directory)
   '((side . left))))

(use-package wc-mode
  :ensure t)

(use-package tmr
  :ensure t)

#+end_src
